﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TfhkaNet;
using TfhkaNet.IF.RD;

namespace BixolonNeotec
{
    public static class ConexionImpresora
    {
        public static bool estado;

        public static Tfhka conectarImpresora()
        {// creacion  de la impresora, en los diferentes puerto que se puede conectar
            Tfhka impresora =   new Tfhka();
            Tfhka impresora1 =  new Tfhka();
            Tfhka impresora2 =  new Tfhka();
            Tfhka impresora3 =  new Tfhka();


            if (impresora.OpenFpctrl("COM2"))
            {
               
                return impresora;
            }
            else if (impresora.OpenFpctrl("COM1"))
            {

                return impresora;

            }
            else if (impresora.OpenFpctrl("COM3"))
            {

                return impresora;
            }
            else {

                try
                {
                    using (StreamWriter w = File.AppendText("Log.txt"))
                    {
                        w.WriteLine(DateTime.Now.ToString() + " - " + "No se pudo conectar");
                    }
                }
                catch {

                    if(!File.Exists("Log.txt")){
                        File.Create("Log.txt");
                } }

                return impresora;
                
            
            }
            

        }

        public static void desconectarImpresora(Tfhka impresora) {

            impresora.CloseFpctrl();
           
        
        }





    }
}
