﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TfhkaNet.IF.RD;

namespace BixolonNeotec
{
    public static  class DocumentoNoVenta
    {

        public static  void DocumentoNoVentaArchivo( Tfhka impresora, string ruta) {

            try
            {
                string texto;

                StreamReader reader = new StreamReader(ruta);
                while ((texto = reader.ReadLine()) != null)
                {




                    var num = texto.Length / 40;
                    var subCadena = new string[num + 1];
                    const int numFijo = 40;

                    for (int i = 0; i < num + 1; i++)
                    {
                        int numCararteres;
                        numCararteres = numFijo;
                        if (num == i)
                            numCararteres = texto.Length - (i) * numFijo;
                        subCadena[i] = texto.Substring(i * numFijo, numCararteres);
                    }

                    for (int i = 0; i < subCadena.Length; i++)
                    {

                        impresora.SendCmd("800" + subCadena[i]);
                    }
                }
                impresora.SendCmd("810");

                reader.Close();
            }
            catch (Exception al)
            {

                using (StreamWriter w = File.AppendText("Log.txt"))
                {
                    w.WriteLine(DateTime.Now.ToString() + " - " + "-Error en el documento no venta");
                }
            }
            }


            public  static  void LineaNoVenta(Tfhka impresora, string texto){
            
            impresora.SendCmd("80$"+texto);
            
            
            }


            public static void DatosProgramacion(Tfhka impresora)
            {

                impresora.SendCmd("D" );


            }




    }
}
