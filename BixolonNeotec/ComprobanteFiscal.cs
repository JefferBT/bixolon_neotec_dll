﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TfhkaNet.IF.RD;
namespace BixolonNeotec
{
    public static class ComprobanteFiscal
    {
        public static bool AbrirFactura(Tfhka impresora, int modo, int tipoFactura, int copias,
            string logo, string densidad,
            string sucursal, string caja, string ncf,
            string razonSocialComprador, string rncComprador,
            string ncfDeReferencia)
        {

            string modoFactura, tipoDeFactura;

            #region Modo de la impresora
            if (modo != 0)
            {// Modo FastFood
                modoFactura = "PJ3200";

            }
            else
            {
                //  Modo Retail;
                modoFactura = "PJ3201";
            }
            #endregion
                    
            #region tipo de factura
            switch (tipoFactura)
            {

                case 1:
                    tipoDeFactura = "/0"; // Consumidor final
                   
                    break;

                case 2:
                    tipoDeFactura = "/1"; // Credito Fiscal y Gubernamental
                    break;

                case 3:
                    tipoDeFactura = "/2";// Nota de Credito Consumidor Final
                    break;

                case 4:
                    tipoDeFactura = "/3";// Nota de Credito Fiscal y Gubernamental
                    break;

                case 6:
                    tipoDeFactura = "/4";// Credito Fiscal Exoneracion de ITBIS(Regimen Especial)
                    break;

                case 8:
                    tipoDeFactura = "/5";// Nota de Credito Exoneracion de ITBIS (Regimen Especial)
                    break;

                default:
                    tipoDeFactura = "/0";// Consumidor final
                    break;


            }

            #endregion

            try
            {

                #region  manejador de copias  56 siempre fijo +numeor de 0 al 99, que indica la cantidad de copias. 
                //por estanda los dejamos que se puedan hacer maximo 20 copias
                if (copias < 21)
                {

                    if (copias > 9)
                    {

                        //  impresora.SendCmd("PJ56" + copias);
                        impresora.SendCmd("PJ5601");

                    }
                    else
                    {

                       //impresora.SendCmd("PJ560" + copias);
                        impresora.SendCmd("PJ560" + 00);


                    }
                }
                else {

                   impresora.SendCmd("PJ5601");

                }

                #endregion
                //impresora.SendCmd("PJ5601");

                impresora.SendCmd(modoFactura); // Modo de factura

                // si la factura  es nota de credito, mandara su NCF Afectado
                if (tipoFactura == 3 || tipoFactura == 4 || tipoFactura == 8)
                {
                    impresora.SendCmd("iF0" + ncfDeReferencia);


                }

                impresora.SendCmd("F" + ncf);// NCF

                if (rncComprador != "")
                {
                    impresora.SendCmd("iR0" + rncComprador);// RNC
                }

                if (razonSocialComprador != "")
                {
                    if (razonSocialComprador.Length < 40)
                        impresora.SendCmd("iS0" + razonSocialComprador); // Razón Social

                    // para que  acepte dos lineas de razon social ,si el nombre es muy grande
                    if (razonSocialComprador.Length > 40 && razonSocialComprador.Length <= 80)
                    {
                        impresora.SendCmd("i00" + razonSocialComprador.Substring(20, 40));
                        impresora.SendCmd("iS0" + razonSocialComprador.Substring(40, razonSocialComprador.Length));
                    }
                }
                else
                {

                    impresora.SendCmd("iS0" + "Cliente Contado");

                }



                //luego de mandarle los datos de la factura del cliente y ncfs, se manda el tipo
                impresora.SendCmd(tipoDeFactura);// Tipo de Factura


                return true;
            }
            catch (Exception e)
            {
                // si existe un error que lo mande al log
                using (StreamWriter w = File.AppendText("Log.txt"))
                {
                    w.WriteLine(DateTime.Now.ToString() + " - " + e);
                }
                return false;

            }
        }

        public static bool EnviarItem(Tfhka impresora, string cantidad,string descripcion, string precio, string tasaImpuesto)
        {
            bool estado=false;
            char impuesto;

            #region Tasas de Impuestos
            switch (tasaImpuesto)
            {

                case "10000":

                    impuesto = ' ';


                    break;

                case "000":

                    impuesto = ' ';


                    break;

                case "0000":

                    impuesto = ' ';


                    break;

                case "E":

                    impuesto = ' ';
                    break;

                case "1600":

                    impuesto = '!';


                    break;

                case "1300":

                    impuesto = '%';
                    break;

                case "1800":

                    impuesto = '"';
                    break;


                case "1100":

                    impuesto = '$';
                    break;


                case "0800":

                    impuesto = '#';
                    break;

                default:
                    impuesto = '"';
                    break;

            }

            #endregion


            try
            {

                estado =   // impresora.SendCmd(" 000000002000001000PLU Exento Pancitu");

               // con codigo 
               //impresora.SendCmd(impuesto + int.Parse(precio).ToString("0000000000") +  int.Parse(cantidad).ToString("000000000").Substring(0,8)+"|"+codigo+"|"+descripcion);
               //con unidad y codigo
               impresora.SendCmd(impuesto + int.Parse(precio).ToString("0000000000") + int.Parse(cantidad).ToString("000000000").Substring(0, 8) +  descripcion);

  
            return estado;
            }
            catch (Exception ex)
            {


                using (StreamWriter w = File.AppendText("Log.txt"))
                {
                    w.WriteLine(DateTime.Now.ToString() + " - " + ex);
                }
                return estado;


            }



        }


        public static bool AnularItem(Tfhka impresora, string cantidad, string descripcion, string precio, string tasaImpuesto)
        {
            bool estado = false;
            char impuesto;

            #region Tasas de Impuestos
            switch (tasaImpuesto)
            {

                case "10000":

                    impuesto = ' ';


                    break;

                case "E":

                    impuesto = ' ';
                    break;

                case "1600":

                    impuesto = '¡';


                    break;

                case "1300":

                    impuesto = '¥';
                    break;

                case "1800":

                    impuesto = '¢';
                    break;


                case "1100":

                    impuesto = '¤';
                    break;


                case "0800":

                    impuesto = '£';
                    break;

                default:
                    impuesto = '"';
                    break;

            }

            #endregion




            try
            {

                estado =   // impresora.SendCmd(" 000000002000001000PLU Exento Pancitu");

               // con codigo 
                    //impresora.SendCmd(impuesto + int.Parse(precio).ToString("0000000000") +  int.Parse(cantidad).ToString("000000000").Substring(0,8)+"|"+codigo+"|"+descripcion);
                    //con unidad y codigo
               impresora.SendCmd(impuesto + int.Parse(precio).ToString("0000000000") + int.Parse(cantidad).ToString("000000000").Substring(0, 8) + descripcion);


                return estado;
            }
            catch (Exception ex)
            {


                using (StreamWriter w = File.AppendText("Log.txt"))
                {
                    w.WriteLine(DateTime.Now.ToString() + " - " + ex);
                }
                return estado;


            }



        }

        public static bool DescuentoItem(Tfhka impresora, string monto)
        {
           
       
            try
            {

                monto = monto.Replace(".", " ").Replace(",", " ");


                if (monto.Length > 2)
                {

                    impresora.SendCmd("q-" + int.Parse(monto).ToString("00000000"));
                }

            }
            catch (Exception ex)
            {


                using (StreamWriter w = File.AppendText("Log.txt"))
                {
                    w.WriteLine(DateTime.Now.ToString() + " - " + ex);
                }


            }



            return true;
        }

        public static bool RecargoItem(Tfhka impresora, string monto)
        {


            try
            {

                monto = monto.Replace(".", " ").Replace(",", " ");


                if (monto.Length > 2)
                {

                    impresora.SendCmd("q+" + int.Parse(monto).ToString("00000000"));
                }

            }
            catch (Exception ex)
            {


                using (StreamWriter w = File.AppendText("Log.txt"))
                {
                    w.WriteLine(DateTime.Now.ToString() + " - " + ex);
                }


            }



            return true;
        }

        public static void CerrarDocumento(Tfhka impresora)
        {
            impresora.SendCmd("199");

        }



        public static void Subtotal(Tfhka impresora) {

            impresora.SendCmd("3");
        
        }

        public static void DescuentoSubtotal(Tfhka impresora,string Monto){

            Monto = Monto.Replace(".", " ").Replace(",", " ");
           
 
            if (Monto.Length > 2)
            {

                impresora.SendCmd("q*" + int.Parse(Monto).ToString("000000000"));
            }
        }

        public static void RecargoSubtotal(Tfhka impresora, string Monto)
        {
            Monto = Monto.Replace(".", " ").Replace(",", " ");

            if (Monto.Length > 2)
            {
                impresora.SendCmd("q#" + int.Parse(Monto).ToString("00000000"));
            }
        }


        public static void Total(Tfhka impresora)
        {

            impresora.SendCmd("101");

        }

        public static void Cancelar(Tfhka impresora) {

            impresora.SendCmd("7");
        }

        public static bool Pagar(Tfhka impresora, string monto , string tipo){
            bool estado = false;

            estado = impresora.SendCmd("2"+tipo+double.Parse(monto).ToString("000000000000"));
            
            return estado;

        }

        public static void Usar10Ley(Tfhka impresora)
        {

            impresora.SendCmd("l1");

            

        }

        public  static void Copia(Tfhka impresora){

            impresora.SendCmd("RU");
         
         
        }


        public static void Comentarios(Tfhka impresora ,   string comentarios)
        {
            try
            {
                var num = comentarios.Length / 62;
                var subCadena = new string[num + 1];
                const int numFijo = 62;
            
                for (int i = 0; i < num + 1; i++)
                {
                    int numCararteres;
                    numCararteres = numFijo;
                    if (num == i)
                        numCararteres = comentarios.Length - (i) * numFijo;
                    subCadena[i] = comentarios.Substring(i * numFijo, numCararteres);
                }

                for (int i = 0; i < subCadena.Length; i++)
                {
                    
                  impresora.SendCmd("@"+subCadena[i]);
                }
            }
            catch (Exception al){
            
                using (StreamWriter w = File.AppendText("Log.txt"))
                {
                    w.WriteLine(DateTime.Now.ToString() + " - " + "-Error en la linea de comentario");
                }
            }
        }

        
   
        // Datos de consulta
        public static double MontoApagar = 0.0;
        public static string TipoDeDocumento = "";
        public static double subtotal = 0.0;
        public static void ObtenerinformacionComprobante(Tfhka impresora) { 
         S2PrinterData datos = impresora.getS2PrinterData();

     
           
               MontoApagar= datos.getAmountPayable();
               TipoDeDocumento = datos.getTypeDocument().ToString();
               subtotal = datos.getSubTotalBases();
              
        } 



    }


}
