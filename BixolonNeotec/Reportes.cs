﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TfhkaNet.IF.RD;
using TfhkaNet.IF.RD.DGII;

namespace BixolonNeotec
{
   public static class Reportes
    {
        public static bool estado;
        public static bool reporteZ(Tfhka impresora) {

            try
            {
                estado = impresora.SendCmd("I0Z0");

                if (estado != false)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }catch(Exception e){

                using (StreamWriter w = File.AppendText("Log.txt"))
                {
                    w.WriteLine(DateTime.Now.ToString() + " - " +e);
                }
                return false;
            }
        }

        public static bool reporteZ2(Tfhka impresora)
        {

         
            estado = impresora.SendCmd("I1Z");

            if (estado != false)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        public static bool reporteX(Tfhka impresora)
        {


            estado = impresora.SendCmd("I0X");

            if (estado != false)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        public static bool reporteX2(Tfhka impresora)
        {


            estado = impresora.SendCmd("I1X");

            if (estado != false)
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        public static void S1(Tfhka impresora) {

            S1PrinterData datos = impresora.getS1PrinterData();

            StreamWriter sw = new StreamWriter(@"C:\Fiscal\DocumentoNoVenta.txt");

            
            sw.WriteLine("Status : S1");
            sw.WriteLine("Número de cajero: "+datos.getCashierNumber());
            sw.WriteLine("Subtotal de ventas: "+datos.getSubTotalSalesClientFinal());
            sw.WriteLine("Número del último documento fiscal: "+datos.getLastNumberTransactionFiscal());
            sw.WriteLine("Subtotal de notas crédito en el día: "+datos.getQuantityTransactionToday());
            sw.WriteLine("Número del último documento no fiscal: "+datos.getSubTotalNoteCreditClientFinal());
            sw.WriteLine("Contador de reportes de aquditoría: "+datos.getAuditReportsCounter());
            sw.WriteLine("RNC: " +datos.getRNC());
            sw.WriteLine("Número del registro de la máquina: "+datos.getRegisteredMachineNumber());
            sw.WriteLine("Fecha y hora actual de la impresora: "+datos.getCurrentPrinterDateTime());
            sw.WriteLine("Contador de cierres en el día: "+datos.getDailyClosureCounter());
            sw.WriteLine("Cantidad de documentos no ficales: "+datos.getQuantityNonFiscalDocuments());
            sw.WriteLine("Subtotal de notas de crédito con crédito fiscal: "+datos.getSubTotalNoteCreditCreditFiscal());
            sw.WriteLine("Subtotal de ventas con crédito fiscal: "+datos.getSubTotalSalesCreditFiscal());
        
            sw.Close();


        }


        public static void S2(Tfhka impresora)
        {

            S2PrinterData datos = impresora.getS2PrinterData();

            StreamWriter sw = new StreamWriter(@"C:\Fiscal\DocumentoNoVenta.txt");
           

            sw.WriteLine("Status : S2");
            sw.WriteLine("Monto por pagar: " + datos.getAmountPayable());
            sw.WriteLine("Data Dummy: " + datos.getDataDummy());
            sw.WriteLine("Número de pagos realizados: " + datos.getNumberPaymentsMade());
            sw.WriteLine("Cantidad de artículos: " + datos.getQuantityArticles());
            sw.WriteLine("Subtotal de bases imponibles: " + datos.getSubTotalBases());
            sw.WriteLine("Subtotal de impuestos: " + datos.getSudTotalTax());
            sw.WriteLine("Tipo de documento: " + datos.getTypeDocument());
    

            sw.Close();


        }

        public static void S3(Tfhka impresora)
        {
            S3PrinterData datos = impresora.getS3PrinterData();


            StreamWriter sw = new StreamWriter(@"C:\Fiscal\DocumentoNoVenta.txt");

            sw.WriteLine("Status: S3");
            sw.WriteLine("Impuesto tasa 1: "+datos.getTax1());
            sw.WriteLine("Impuesto tasa 2: "+datos.getTax2());
            sw.WriteLine("Impuesto tasa 3: "+datos.getTax3());
            sw.WriteLine("Impuesto tasa 4: "+datos.getTax4());
            sw.WriteLine("Impuesto tasa 5: "+datos.getTax5());
            sw.WriteLine("Tipo de impuesto tasa 1: "+datos.getTypeTax1());
            sw.WriteLine("Tipo de impuesto tasa 2: "+datos.getTypeTax2());
            sw.WriteLine("Tipo de impuesto tasa 3: "+datos.getTypeTax3());
            sw.WriteLine("Tipo de impuesto tasa 4: "+datos.getTypeTax4());
            sw.WriteLine("Tipo de impuesto tasa 5: " + datos.getTypeTax5());

            System.Int32[] banderas = impresora.getS3PrinterData().getSystemFlags0to63();

             int j = banderas.Length;

             for (int i = 1; i < j; i++){
                 sw.WriteLine("Flag "+i+": "+banderas[i]) ;
             }

             sw.Close();
            

        }

        public static void S4(Tfhka impresora)
        {
            S4PrinterData datos = impresora.getS4PrinterData();


            StreamWriter sw = new StreamWriter(@"C:\Fiscal\DocumentoNoVenta.txt");

            sw.WriteLine("Status: S4");


            double[] medios; //Crea un arreglo de datos tipo double

            //Se extraen todos los medios de pago y se guardan en el arreglo
            medios = datos.getListCumulaMountsMeansPayments();

            int j = medios.Length;

                //Se llenan los valores de medio de pago, separando estos del arreglo medios.
            for (int i = 1; i <= j; i++)
            {
                sw.WriteLine("Medio de Pago"+i+": "+medios[i-1]);
            
            }

            sw.Close();


        }

        public static void S5(Tfhka impresora)
        {
            S5PrinterData datos = impresora.getS5PrinterData();


            StreamWriter sw = new StreamWriter(@"C:\Fiscal\DocumentoNoVenta.txt");

            sw.WriteLine("Status: S5");
            sw.WriteLine("Espacio libre en la memoria de auditoría:"+datos.getDisponyCapacityMemoryAudit());
            sw.WriteLine( "Número de memoria de auditoría:"+datos.getNumberMemoryAudit());
            sw.WriteLine("Capacidad de la memoria de auditoría:"+datos.getCapacityTotalMemoryAudit());
            sw.WriteLine("Número de documentos registrados:"+datos.getNumberDocumentRegisters());
            sw.WriteLine("Serial de la máquina:"+datos.getRegisteredMachineNumber());
            sw.WriteLine("RNC"+datos.getRNC());

            sw.Close();

        }

    
        




    }
}
